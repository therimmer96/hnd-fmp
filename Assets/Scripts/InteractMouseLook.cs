﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractMouseLook : MonoBehaviour
{
	public Transform cameraTrans;

	public KeyCode interactKey = KeyCode.F;

	public Texture2D gazeCursorTexture;
	public CursorLockMode gazeLockMode;
	private bool inGazeMode;

	public Vector2 turnSpeed;
	public float gazeTurnMult;

	// Use this for initialization
	void Start()
	{
		#region Platform Dependented Gaze Mode
		gazeLockMode = CursorLockMode.Confined;
#if UNITY_EDITOR
		gazeLockMode = CursorLockMode.None;
#endif
		#endregion

		DisableGazeMode();
	}

	// Update is called once per frame
	void Update()
	{
		if(Input.GetKeyDown(interactKey))
		{
			EnableGazeMode();
		}
		else if(Input.GetKeyUp(interactKey))
		{
			DisableGazeMode();
		}

		Vector2 rotateAngle = new Vector2(Input.GetAxis("Mouse X") * turnSpeed.x, Input.GetAxis("Mouse Y") * turnSpeed.y);

		if(inGazeMode)
		{
			rotateAngle *= gazeTurnMult;
		}

		transform.Rotate(transform.up, rotateAngle.x);
		cameraTrans.Rotate(Vector3.left, rotateAngle.y);
	}

	private void EnableGazeMode()
	{
		Cursor.lockState = gazeLockMode;
		Cursor.visible = true;
		Cursor.SetCursor(gazeCursorTexture, new Vector2(128, 128), CursorMode.Auto);

		inGazeMode = true;
	}

	private void DisableGazeMode()
	{
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);

		inGazeMode = false;
	}
}
