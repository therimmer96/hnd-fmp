﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Rigidbody))]
public class ShipMovement : NetworkBehaviour
{
	Rigidbody rb;

	public float MainEngineForce;
	public float RCSEngineForce;

	public float MainEngineMaxSpeed;
	public float RCSEngineMaxSpeed;

	// Use this for initialization
	void Start()
	{
		rb = GetComponent<Rigidbody>();
	}

	// Update is called once per frame
	void FixedUpdate()
	{
		if(isLocalPlayer)
		{
			rb.AddRelativeForce(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

			if(Input.GetKey(KeyCode.LeftShift))
			{
				rb.AddRelativeForce(-rb.velocity.normalized);
			}
		}
	}
}
