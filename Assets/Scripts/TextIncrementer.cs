﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextIncrementer : MonoBehaviour
{
	public Text display;
	public string textToDisplay = "Button clicked {0} times";

	private int count;

	public void Start()
	{
		display.text = string.Format(textToDisplay, count);
	}

	public void Increment()
	{
		count++;
		display.text = string.Format(textToDisplay, count);
	}
}
